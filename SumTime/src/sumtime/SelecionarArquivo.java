/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sumtime;

import java.io.*;
import javax.swing.JFileChooser;
import org.jdom2.JDOMException;

class SelecionarArquivo{
    public SelecionarArquivo() throws FileNotFoundException, IOException, JDOMException{
        JFileChooser chooser=new  JFileChooser();
        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            File f = chooser.getSelectedFile();
            Analise.read(f);
        }else{
            new TelaInicial().setVisible(true);
        }
    }
}
