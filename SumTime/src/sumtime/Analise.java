
package sumtime;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author sumTime Group
 */
public class Analise {
    
    private static String cursoNome;
    private static String nomeArquivo;
    private ArrayList<Atividade> atividades;
    private float totalHoras;
    private static HashMap defaults = new HashMap();
    
    /**
     * 
     * Quando cria-se uma nova analise os padrões do XML são carregados para uma HashMap
     * @param curso
     * @throws JDOMException
     * @throws IOException 
     */
    public Analise() throws JDOMException, IOException{
       this.atividades = new ArrayList();
       this.totalHoras = 0;
       this.cursoNome = "Ciência da Computação";
       this.carregarDefault();
    }
    
    /**
     * 
     * Metodo que carrega os padrões do XML para o atributo defaults
     * Esse Metodo é private pois vai ser usado somente no construtor da classe
     * @throws JDOMException
     * @throws IOException 
     */
    private void carregarDefault() throws JDOMException, IOException{
        List<List<String>> output = Analise.readDefault();
        for(int i = 0; i<output.size();i+=2){
            
            HashMap modalidadeDefault = new HashMap();
            
            for(int j = 0;j < output.get(i+1).size(); j+=3){
                Modalidade modalidade;
                modalidade = new Modalidade(output.get(i+1).get(j+1),Float.parseFloat(output.get(i+1).get(j+2)));
                modalidadeDefault.put(output.get(i+1).get(j), modalidade); 
            }
            this.defaults.put(output.get(i).get(0), modalidadeDefault);
        }
    }
    
    public static HashMap getHash(){
        return (HashMap)defaults.get(cursoNome); 
    }
    
    /**
     * 
     * @param tipoAtividade
     * @return modalidaDefaul
     */
    public  Modalidade getDefault(String tipoAtividade){
        return (Modalidade)((HashMap)this.defaults.get(this.cursoNome)).get(tipoAtividade);
    }
    /**
     * 
     * @param atividade
     * @return boolean    
     */
    public boolean adicionarAtividade(Atividade atividade){
        if(atividades.add(atividade)){
            totalHoras=+atividade.getHoras();
            return true;
            
        }
        return false;
    }
    
    static void setCurso(String curso){
        Analise.cursoNome = curso;
    }
    
    /**
     * Método responsável pela geração do arquivo XML
     * 
    */
    public static void write(ArrayList<String> output, String path) throws FileNotFoundException, IOException{
       Document doc = new Document();

       Element root = new Element("lista");
       Attribute curso = new Attribute("curso",output.get(0));
       root.setAttribute(curso);
       
       for(int i=1; i<output.size();){
           Element atividade = new Element("atividade");
           
           Element nome = new Element("nome");
           nome.setText(output.get(i));
           atividade.addContent(nome);
           i++;
           Element tipo = new Element("tipo");
           tipo.setText(output.get(i));
           atividade.addContent(tipo);
           i++; 
           Element modalidade = new Element("modalidade");
           modalidade.setText(output.get(i));
           atividade.addContent(modalidade);
           i++;
           Element horas = new Element("horas");
           horas.setText(output.get(i));
           atividade.addContent(horas);
           i++;
           root.addContent(atividade);
       }

       doc.setRootElement(root);

       XMLOutputter outputter = new XMLOutputter();
       outputter.setFormat(Format.getPrettyFormat());
       outputter.output(doc, new FileWriter(path));
       
    }
    

    /**
     * Método responsavel pela leitura do arquivo XML do aluno
     */
    public static void read(File arquivo) throws JDOMException, IOException{
        nomeArquivo = arquivo.getAbsolutePath();
        
        SAXBuilder reader = new SAXBuilder();
        Document doc = reader.build(new File(nomeArquivo));
        
        List<Attribute> curso = doc.getRootElement().getAttributes();
        String s = curso.toString();
        Pattern p = Pattern.compile("\"(.*?)\"");
        Matcher m = p.matcher(s); 
        if (m.find()) {
            cursoNome = m.group(1);
        }
        
        Element lista = doc.getRootElement();
        List<Element> atividades = lista.getChildren();
        
        ArrayList<String> act = new ArrayList<String>();
        act.add(cursoNome);
        for(Element e: atividades){
             act.add(e.getChildText("nome"));
             act.add(e.getChildText("tipo"));
             act.add(e.getChildText("modalidade"));
             act.add(e.getChildText("horas"));
        }
        SegundaTela segtela = new SegundaTela();
        segtela.setVisible(true);
        segtela.setaCurso(act.get(0));
        for(int i=1; i<act.size();){
            ArrayList<String> activ = new ArrayList();
            activ.add(act.get(i));
            i++;
            activ.add(act.get(i));
            i++;
            activ.add(act.get(i));
            i++;
            activ.add(act.get(i));
            i++;
            segtela.gerarLinhaInput(activ);
        }
        
    }
    
    /**
     * Método responsável pela leitura do arquivo XML default (informado pelo prof)
     */
    public static  List<List<String>> readDefault() throws JDOMException, IOException{
        SAXBuilder reader = new SAXBuilder();
        Document doc = reader.build(new File("default.xml"));
        
        Element lista = doc.getRootElement();
         
        List<List<String>> output = new ArrayList<>();
        List curso = lista.getChildren("curso");
        Iterator i = curso.iterator();
        while (i.hasNext()) { 
            ArrayList<String> cursoEHoras = new ArrayList<String>();
            Element cursO = (Element) i.next();
            cursoEHoras.add(cursO.getChildText("name"));
            cursoEHoras.add(cursO.getChildText("hrs"));
            ArrayList<String> cursoAtividades = new ArrayList<String>();
            List atividade = cursO.getChildren("atividade");
            Iterator j = atividade.iterator();
            while (j.hasNext()){
                Element activ = (Element) j.next();
                cursoAtividades.add(activ.getChildText("tipo"));
                cursoAtividades.add(activ.getChildText("categoria"));
                cursoAtividades.add(activ.getChildText("horas"));
            }
            output.add(cursoEHoras);
            output.add(cursoAtividades);
        }
        return output;
    }
}

