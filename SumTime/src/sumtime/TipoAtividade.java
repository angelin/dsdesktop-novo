/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sumtime;

/**
 *
 * @author sumTime Group
 */
public class TipoAtividade {
    
    /**
     * Atributos da Classe
     * nome é a atividade e máximoHoras é o máximo de horas da atividade.
     */
    private String nome;
    private float maximoHoras;
    
    /**
     * Método Construtor - Primeiro sem parâmetros.
     */
    public TipoAtividade(){
        this.nome = "";
        this.maximoHoras = 0;
    }
    
    /**
     * Método Construtor - Segundo com parâmetros nome da atividade e maximoHoras da atividade.
     * @param nome
     * @param maximoHoras 
     */
    public TipoAtividade(String nome, float maximoHoras){
        this.nome = nome;
        this.maximoHoras = maximoHoras;
    }
    
    /**
     * Método Set Nome
     * @param nome passa o nome da atividade.
     */
    public void setNome(String nome){
        this.nome = nome;
    }
    
    /**
     * Método Get Nome
     * @return o nome da atividade
     */
    public String getNome(){
        return nome;
    }
    
    /**
     * Método Set Máximo Horas
     * @param maximoHoras seta o máximo de horas da atividade
     */
    public void setMaximoHoras(float maximoHoras){
        this.maximoHoras = maximoHoras;
    }
    
    /**
     * Método Get Máximo Horas
     * @return o máximo de horas da atividade.
     */
    public float getMaximoHoras()   {
        return maximoHoras;
    }
}
