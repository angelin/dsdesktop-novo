/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sumtime;

/**
 *
 * @author sumTime Group
 */
class Curso {
    
    private String nome;
    private float horasNecessarias;
    
    /**
     * 
     * @param nome
     * @param horasNecessarias 
     */
    public Curso(String nome, float horasNecessarias){
        this.nome  = nome;
        this.horasNecessarias = horasNecessarias;
    }
    
    /**
     * 
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * 
     * @return horasNecessessarias
     */
    public float getHorasNecessarias() {
        return horasNecessarias;
    }

    /**
     * 
     * @param nome 
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * 
     * @param horasNecessarias 
     */
    public void setHorasNecessarias(float horasNecessarias) {
        this.horasNecessarias = horasNecessarias;
    }
    
    
    
}