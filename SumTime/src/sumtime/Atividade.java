/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sumtime;

/**
 *
 * @author sumTime Group
 */
class Atividade {
    private Modalidade modalidade;
    private float horas;
    private TipoAtividade tipoAtividade;
    private String nome;
    private float maximoHoras;
    
    
    public Atividade(String nomeAtividade){
        this.nome = nomeAtividade;
    }
   
    public void setModalidade(float minimoHoras, String categoria) {
        this.modalidade.setMinimoHoras(minimoHoras);
        this.modalidade.setCategoria(categoria);
    }

    public void setHoras(float horas) {
        this.horas = horas;
    }

    public void setTipoAtividade(String nome, float MaximoHoras) {
        this.tipoAtividade.setNome(nome);
        this.tipoAtividade.setMaximoHoras(MaximoHoras);
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Modalidade getModalidade() {
        return modalidade;
    }

    public float getHoras() {
        return horas;
    }

    public TipoAtividade getTipoAtividade() {
        return tipoAtividade;
    }

    public String getNome() {
        return nome;
    }

    public float getMaximoHoras() {
        return maximoHoras;
    }
}
